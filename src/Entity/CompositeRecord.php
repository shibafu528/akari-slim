<?php
declare(strict_types=1);

namespace Akari\Entity;

/**
 * Chinachu上の録画情報と、DB上の視聴記録を結合して扱うクラス
 * @package Akari\Entity
 */
class CompositeRecord
{
    private ChinachuRecord $chinachuRecord;
    private ?Record $record;
    private ?WatchLog $watchLog;

    public function __construct(ChinachuRecord $chinachuRecord, ?Record $record, ?WatchLog $watchLog)
    {
        $this->chinachuRecord = $chinachuRecord;
        $this->record = $record;
        $this->watchLog = $watchLog;
    }

    public function __get($name)
    {
        if ($name === 'watched') {
            if ($this->watchLog !== null) {
                return $this->watchLog->isWatched();
            }
            if ($this->record !== null) {
                return $this->record->isWatched();
            }
            return false;
        }

        return $this->chinachuRecord->{$name};
    }

    public function __isset($name)
    {
        return $name === 'watched' || isset($this->chinachuRecord->{$name});
    }

    /**
     * @param CompositeRecord|ChinachuRecord|null $other
     * @return bool
     */
    public function isSame($other): bool
    {
        if ($other === null) {
            return false;
        }
        if ($other instanceof CompositeRecord) {
            $other = $other->chinachuRecord;
        }
        return $this->chinachuRecord->isSame($other);
    }

    /**
     * @return Record|null
     */
    public function getRecord(): ?Record
    {
        return $this->record;
    }

    public function getWatchLogId(): array
    {
        return $this->chinachuRecord->getWatchLogId();
    }
}
