<?php
declare(strict_types=1);

namespace Akari\Entity\Concern;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

trait AutoTimestamp
{
    /**
     * @ORM\PrePersist
     */
    public function autoTimestampOnPrePersist()
    {
        if ($this->createdAt === null) {
            $this->createdAt = new DateTime();
        }
        if ($this->updatedAt === null) {
            $this->updatedAt = new DateTime();
        }
    }

    /**
     * @ORM\PreUpdate
     */
    public function autoTimestampOnPreUpdate()
    {
        $this->updatedAt = new DateTime();
    }
}
