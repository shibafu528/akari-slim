<?php
declare(strict_types=1);

namespace Akari\Entity;

use Akari\Entity\Concern\AutoTimestamp;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="filters")
 * @ORM\HasLifecycleCallbacks
 */
class Filter
{
    use AutoTimestamp;

    const TARGET_TITLE = 0;
    const MATCHER_PARTIAL = 0;
    const MATCHER_EXACT = 1;
    const MATCHER_REGEX = 2;

    /** 検査対象の表示名 */
    const TARGET_LABEL = [
        0 => '番組名'
    ];

    /** 一致条件の表示名 */
    const MATCHER_LABEL = [
        0 => '部分一致',
        1 => '完全一致',
        2 => '正規表現'
    ];

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $userId;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $keyword;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $target;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $matcher;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="create_at")
     */
    private $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="update_at")
     */
    private $updatedAt;

    /**
     * @param int $userId
     */
    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return int
     */
    public function getTarget(): int
    {
        return $this->target;
    }

    /**
     * @param int $target
     */
    public function setTarget(int $target): void
    {
        $this->target = $target;
    }

    /**
     * @return int
     */
    public function getMatcher(): int
    {
        return $this->matcher;
    }

    /**
     * @param int $matcher
     */
    public function setMatcher(int $matcher): void
    {
        $this->matcher = $matcher;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * 指定した値がこのフィルタ条件とマッチするか判定
     * @param string|null $value
     * @return bool
     */
    public function match(?string $value): bool
    {
        switch ($this->getMatcher()) {
            case self::MATCHER_PARTIAL:
                return mb_strpos($value, $this->getKeyword()) === false;
            case self::MATCHER_EXACT:
                return $value !== $this->getKeyword();
            case self::MATCHER_REGEX:
                return preg_match('/' . $this->getKeyword() . '/u', $value) !== 1;
        }

        return false;
    }
}
