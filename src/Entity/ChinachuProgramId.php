<?php
declare(strict_types=1);

namespace Akari\Entity;

use Symfony\Component\String\Exception\RuntimeException;

/**
 * ChinachuのProgram IDをNetwork, Service, Event IDに分解したもの
 */
class ChinachuProgramId
{
    /**
     * Chinachu β のIDパターン
     *
     * {type}{service_id}-(_){radix16_event_id}
     */
    private const CHINACHU_BETA_ID_PATTERN = '/^(?P<type>gr|bs|cs|sky)(?P<sid>\d+)-_?(?P<eid>[0-9a-z]+)$/';

    /**
     * Chinachu γ のIDパターン
     *
     * {radix16_composite_id}(-{radix16_start_at_timestamp})
     */
    private const CHINACHU_GAMMA_ID_PATTERN = '/^(?P<id>[0-9a-z]+)(-\d+)?$/';

    /**
     * 不明なネットワークID
     */
    private const NETWORK_ID_UNKNOWN = 0;

    /**
     * BSのネットワークID
     */
    private const NETWORK_ID_BS = 4;

    private const MIRAKURUN_ID_DIVISOR = 100_000;

    public int $networkId;
    public int $serviceId;
    public int $eventId;

    public function __construct(string $id)
    {
        if (preg_match(self::CHINACHU_BETA_ID_PATTERN, $id, $matches)) {
            $type = $matches['type'];
            $this->serviceId = (int)$matches['sid'];
            $this->networkId = self::betaChannelIdToNetworkId($type, $this->serviceId);
            $this->eventId = (int)base_convert($matches['eid'], 36, 10);
        } elseif (preg_match(self::CHINACHU_GAMMA_ID_PATTERN, $id, $matches)) {
            $compositeId = (int)base_convert($matches['id'], 36, 10);
            $this->networkId = intdiv($compositeId, (self::MIRAKURUN_ID_DIVISOR * self::MIRAKURUN_ID_DIVISOR));
            $this->serviceId = intdiv($compositeId, self::MIRAKURUN_ID_DIVISOR) % self::MIRAKURUN_ID_DIVISOR;
            $this->eventId = $compositeId % self::MIRAKURUN_ID_DIVISOR;
        } else {
            throw new RuntimeException('invalid format id');
        }
    }

    public function __toString(): string
    {
        return '{' . implode(', ', [$this->networkId ?? 'null', $this->serviceId, $this->eventId]) . '}';
    }

    private static function betaChannelIdToNetworkId(string $type, int $sid): int
    {
        if ($type === 'bs') {
            return self::NETWORK_ID_BS;
        }
        // BS以外も対応表が作れればマッピングできるけど、難しそう。
        return self::NETWORK_ID_UNKNOWN;
    }
}
