<?php
declare(strict_types=1);

namespace Akari\Entity;

use Akari\Entity\Concern\AutoTimestamp;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="records")
 * @ORM\HasLifecycleCallbacks
 */
class Record
{
    use AutoTimestamp;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $programId;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $userId;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $watched;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="create_at")
     */
    private $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="update_at")
     */
    private $updatedAt;

    /**
     * @param string $programId
     * @param int $userId
     */
    public function __construct(string $programId, int $userId)
    {
        $this->programId = $programId;
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getProgramId(): string
    {
        return $this->programId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return bool
     */
    public function isWatched(): bool
    {
        return $this->watched;
    }

    /**
     * @param bool $watched
     */
    public function setWatched(bool $watched): void
    {
        $this->watched = $watched;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
