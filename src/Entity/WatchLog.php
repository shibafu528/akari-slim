<?php
declare(strict_types=1);

namespace Akari\Entity;

use Akari\Entity\Concern\AutoTimestamp;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="watch_logs")
 * @ORM\HasLifecycleCallbacks
 */
class WatchLog
{
    use AutoTimestamp;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="network_id")
     */
    private int $networkId;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="service_id")
     */
    private int $serviceId;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="event_Id")
     */
    private int $eventId;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="start_at")
     */
    private int $startedAt;

    /**
     * @ORM\Column(type="string", name="chinachu_id")
     */
    private string $chinachuId;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="watched")
     */
    private bool $watched;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="create_at")
     */
    private ?DateTime $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="update_at")
     */
    private ?DateTime $updatedAt;

    /**
     * @param int $networkId
     * @param int $serviceId
     * @param int $eventId
     * @param int $startedAt
     */
    public function __construct(int $networkId, int $serviceId, int $eventId, int $startedAt)
    {
        $this->networkId = $networkId;
        $this->serviceId = $serviceId;
        $this->eventId = $eventId;
        $this->startedAt = $startedAt;
        $this->createdAt = null;
        $this->updatedAt = null;
    }

    public function getIdentifier(): string
    {
        return implode(',', [$this->networkId, $this->serviceId, $this->eventId, $this->startedAt]);
    }

    /**
     * @return int|null
     */
    public function getNetworkId(): ?int
    {
        return $this->networkId;
    }

    /**
     * @return int
     */
    public function getServiceId(): int
    {
        return $this->serviceId;
    }

    /**
     * @return int
     */
    public function getEventId(): int
    {
        return $this->eventId;
    }

    /**
     * @return int
     */
    public function getStartedAt(): int
    {
        return $this->startedAt;
    }

    /**
     * @return string
     */
    public function getChinachuId(): string
    {
        return $this->chinachuId;
    }

    /**
     * @param string $chinachuId
     */
    public function setChinachuId(string $chinachuId): void
    {
        $this->chinachuId = $chinachuId;
    }

    /**
     * @return bool
     */
    public function isWatched(): bool
    {
        return $this->watched;
    }

    /**
     * @param bool $watched
     */
    public function setWatched(bool $watched): void
    {
        $this->watched = $watched;
    }
}
