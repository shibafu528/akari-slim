<?php
declare(strict_types=1);

namespace Akari\Entity;

use DateTime;

class ChinachuRecord
{
    /** @var ChinachuProgramId パース済のProgram ID */
    public ChinachuProgramId $id;
    /** @var String Chinachu ID */
    public $programId;
    /** @var String 番組名 */
    public $title;
    /** @var String サブタイトル */
    public $subTitle;
    /** @var int|null 話数 */
    public $episode;
    /** @var DateTime 放送日時 */
    public $date;
    /** @var string 放送チャンネル名 */
    public $channelName;

    /** @var int レコード重複数 (2以上で重複あり) */
    public $duplicatedCount = 1;

    /** @var ChinachuRecord[] Chinachu IDの重複しているレコードのリスト */
    public $conflicts = [];

    /**
     * @param String $programId
     * @param String $title
     * @param String $subTitle
     * @param int|null $episode
     * @param DateTime $date
     * @param string $channelName
     */
    public function __construct(string $programId, string $title, string $subTitle, ?int $episode, DateTime $date, string $channelName)
    {
        $this->id = new ChinachuProgramId($programId);
        $this->programId = $programId;
        $this->title = $title;
        $this->subTitle = $subTitle;
        $this->episode = $episode;
        $this->date = $date;
        $this->channelName = $channelName;
    }

    public function isSame(?ChinachuRecord $other): bool
    {
        return $other !== null && $this->programId == $other->programId && $this->date == $other->date;
    }

    public function getWatchLogId(): array
    {
        return [$this->id->networkId, $this->id->serviceId, $this->id->eventId, $this->date->getTimestamp()];
    }
}
