<?php
declare(strict_types=1);

namespace Akari\Api;

class ChinachuRecordedJson implements Chinachu
{
    private string $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function get(string $endpoint): array
    {
        if ($endpoint !== '/recorded.json') {
            throw new ChinachuException("unsupported endpoint: $endpoint");
        }

        return json_decode(file_get_contents($this->filePath), true);
    }
}
