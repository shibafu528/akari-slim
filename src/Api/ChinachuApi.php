<?php
declare(strict_types=1);

namespace Akari\Api;

class ChinachuApi implements Chinachu
{
    /** @var string */
    private $baseUrl;

    /**
     * @param string|null $baseUrl Chinachu API base URL
     */
    public function __construct(?string $baseUrl = null)
    {
        if (empty($baseUrl)) {
            $this->baseUrl = $_ENV['CHINACHU_API'];
        } else {
            $this->baseUrl = $baseUrl;
        }
    }

    /**
     * ChinachuサーバにGETリクエストを行い、結果として返されたJSONをデコードします。
     * @param string $endpoint エンドポイントURL
     * @return array レスポンスの連想配列
     * @throws ChinachuException レスポンスコードが正常でない場合にスロー。
     */
    public function get(string $endpoint): array
    {
        return $this->requestJson($endpoint);
    }

    /**
     * Chinachuサーバにリクエストを行い、結果として返されたJSONをデコードします。
     * @param string $endpoint エンドポイントURL
     * @param string $method HTTPリクエストメソッド (GET, POST)
     * @param array $post_fields POSTリクエスト時のデータ
     * @return array レスポンスの連想配列
     * @throws ChinachuException レスポンスコードが正常でない場合にスロー。
     */
    private function requestJson(string $endpoint, string $method = 'GET', array $post_fields = []): array
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->baseUrl . $endpoint);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        switch ($method) {
            case 'GET':
                curl_setopt($curl, CURLOPT_HTTPGET, true);
                break;
            case 'POST':
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_fields));
                break;
        }
        $response = curl_exec($curl);

        $header = curl_getinfo($curl);
        $code = $header['http_code'];
        curl_close($curl);
        if ($code >= 400) {
            // on error
            throw new ChinachuException('CURL error : ' . $response, $code);
        }

        return json_decode($response, true);
    }
}
