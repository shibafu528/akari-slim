<?php
declare(strict_types=1);

namespace Akari\Api;

interface Chinachu
{
    /**
     * ChinachuサーバにGETリクエストを行い、結果として返されたJSONをデコードします。
     * @param string $endpoint エンドポイントURL
     * @return array レスポンスの連想配列
     * @throws ChinachuException レスポンスコードが正常でない場合にスロー。
     */
    public function get(string $endpoint): array;
}
