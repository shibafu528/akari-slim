<?php
declare(strict_types=1);

namespace Akari\Api;

/**
 * ChinachuへのAPIアクセスをAPCuにキャッシュするプロキシ
 * @package Akari\Api
 */
class ChinachuCache implements Chinachu
{
    const TTL = 300;

    /** @var ChinachuApi */
    private ChinachuApi $api;

    public function __construct(ChinachuApi $api)
    {
        $this->api = $api;
    }

    public function get(string $endpoint): array
    {
        $key = 'akari::' . __METHOD__ . '(' . $endpoint . ')';
        return $this->retryOnce($key, function () use ($endpoint) {
            return $this->api->get($endpoint);
        }, self::TTL);
    }

    private function retryOnce(string $key, callable $generator, int $ttl)
    {
        // apcu_entryは正常動作できていない場合にE_WARNINGでエラーを出力する。それをWhoopsに引っかからないように捕捉して再試行する。
        $errorCatcher = new class {
            /** @var \Exception|null */
            public $error = null;

            public function try(callable $fn)
            {
                set_error_handler([$this, 'handler'], E_WARNING);
                try {
                    $result = $fn();
                    if ($this->error === null) {
                        return $result;
                    } else {
                        throw $this->error;
                    }
                } finally {
                    restore_error_handler();
                }
            }

            public function handler(int $no, string $message, string $file, int $line): bool
            {
                if (strpos($message, 'apcu_entry()') === 0) {
                    $this->error = new \ErrorException($message, $no, E_WARNING, $file, $line);
                    return true;
                }
                return false;
            }
        };

        try {
            return $errorCatcher->try(function () use ($key, $generator, $ttl) {
                return apcu_entry($key, $generator, $ttl);
            });
        } catch (\ErrorException $e) {
            // 容量不足の可能性があるので、一旦キャッシュを消してみる
            apcu_clear_cache();
            return apcu_entry($key, $generator, $ttl);
        }
    }
}
