<?php
declare(strict_types=1);

namespace Akari\Middleware;

use Akari\Adapter\Smarty;
use Akari\Contract\ViewRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\App;

class SmartyMiddleware implements MiddlewareInterface
{
    /** @var Smarty */
    private $smarty;
    /** @var string */
    private $rootUrl;

    public function __construct(App $app)
    {
        $this->smarty = $app->getContainer()->get(ViewRenderer::class);
        $this->rootUrl = $app->getBasePath();
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->smarty->assign('root_url', $this->rootUrl);
        return $handler->handle($request);
    }
}
