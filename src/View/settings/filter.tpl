<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8"/>
    <title>フィルタ設定</title>
    <link rel="stylesheet" href="{$root_url}/common.css"/>
</head>
<body>
<div class="page-title">
    <h1>フィルタ設定</h1>
    {include file="../elements/title_menu.tpl"}
</div>
<div class="content">
    <p>
        フィルタ設定では、条件を指定して録画データを非表示にすることができます。<br>
        ここでフィルタした録画は、検索でも表示されません。
    </p>
    <form id="form" class="options-form" method="post" action="{$root_url}/settings/filter">
        <input type="hidden" id="filterId" name="filterId" value=""/>
        <input type="hidden" id="delete" name="delete" value="0"/>
        <div class="options">
            <span id="formTitle">登録</span>
            <br/>
            <select id="target" name="target">
                {foreach from=$target_label key=key item=item}
                    <option value="{$key}">{htmlspecialchars($item)}</option>
                {/foreach}
            </select>
            <input type="text" id="keyword" name="keyword" value="" placeholder="キーワード" maxlength="256">
            <select id="matcher" name="matcher">
                {foreach from=$matcher_label key=key item=item}
                    <option value="{$key}">{htmlspecialchars($item)}</option>
                {/foreach}
            </select>
            <input type="submit" value="登録">
            <input id="cancelButton" type="button" value="キャンセル" onclick="cancelEdit(); return false;" style="display: none;">
        </div>
    </form>
    <table class="grid">
        <thead>
        <tr>
            <th>#</th>
            <th>検査対象</th>
            <th>キーワード</th>
            <th>一致条件</th>
            <th>編集</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$data item=item}
            <tr>
                <td>{htmlspecialchars($item->getId())}</td>
                <td>{htmlspecialchars($target_label[$item->getTarget()])}</td>
                <td>{htmlspecialchars($item->getKeyword())}</td>
                <td>{htmlspecialchars($matcher_label[$item->getMatcher()])}</td>
                <td>
                    <a class="btn-watched" href="#" onclick="editFilter({$item->getId()}, '{$item->getKeyword()}', {$item->getTarget()}, {$item->getMatcher()}); return false;">編集</a>
                    <a class="btn-notyet" href="#" onclick="deleteFilter({$item->getId()}); return false;">削除</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="5"><span class="msg-info">まだ登録されていません。</span></td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{$root_url}/js/settings/filter.js" type="text/javascript"></script>
</body>
</html>
