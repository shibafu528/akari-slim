<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8"/>
    <title>ID衝突チェック</title>
    <link rel="stylesheet" href="{$root_url}/common.css"/>
</head>
<body>
<div class="page-title">
    <h1>ID衝突チェック</h1>
    {include file="../elements/title_menu.tpl"}
</div>
<div class="content">
    <form id="form" method="post" action="{$root_url}/records/update">
        <input type="hidden" id="watchLogId" name="watchLogId" value=""/>
        <input type="hidden" id="newValue" name="newValue" value=""/>
        <input type="hidden" id="redirectTo" name="redirectTo" value="/records/conflicts" />
        <table class="table-conflicts">
            <thead>
            <tr>
                <th>タイトル</th>
                <th>放送日時</th>
                <th>旧視聴記録</th>
                <th>視聴記録</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$data key=id item=records}
                <tr id="{$id}" class="table-conflicts-id">
                    <td colspan="4">{$id}</td>
                </tr>
                {foreach from=$records item=rec}
                    <tr>
                        <td>
                            {htmlspecialchars($rec->title)}
                            {if !empty($rec->subTitle) || !empty($rec->episode)}<br>{/if}
                            {if !empty($rec->episode)}<span class="episode"> #{htmlspecialchars($rec->episode)}</span>{/if}
                            {if !empty($rec->subTitle)}<span class="subtitle"> {htmlspecialchars($rec->subTitle)}</span>{/if}
                        </td>
                        <td>{$rec->date->format('Y/m/d H:i:s')}<br><span class="channel-name">{htmlspecialchars($rec->channelName)}</span></td>
                        <td align="center">{if $rec->getRecord() !== null && $rec->getRecord()->isWatched()}<span class="btn-watched" style="pointer-events: none;">視聴済</span>{else}<span class="btn-notyet" style="pointer-events: none;">未視聴</span>{/if}</td>
                        <td align="center">{if $rec->watched}<a class="btn-watched" href="#" onclick="event.preventDefault(); update('{','|implode:$rec->getWatchLogId()}', '0', '{$id}')">視聴済</a>{else}<a class="btn-notyet" href="#" onclick="event.preventDefault(); update('{','|implode:$rec->getWatchLogId()}', '1', '{$id}')">未視聴</a>{/if}</td>
                    </tr>
                {/foreach}
                {foreachelse}
                <tr>
                    <td colspan="5"><span class="msg-info">録画データが見つかりませんでした</span></td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </form>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{$root_url}/js/records/conflicts.js" type="text/javascript"></script>
</body>
</html>
