<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8"/>
    <title>録画一覧</title>
    <link rel="stylesheet" href="{$root_url}/common.css"/>
</head>
<body>
<div class="page-title">
    <h1>録画一覧</h1>&nbsp;<small>Powered by Slim 4</small>
    {include file="../elements/title_menu.tpl"}
</div>
<div class="content">
    <p>
        <b>{$count_all}</b> 件の録画データ, <b>{$count_notyet}</b> 件が未視聴です。
        <a href="https://twitter.com/share" class="twitter-share-button" data-url=" " data-text="{$count_all} 件の録画データのうち、{$count_notyet} 件が未視聴です。" data-hashtags="akariRec" data-dnt="true">Tweet</a>
        <script>{literal}!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');{/literal}</script>
    </p>
    <form id="searchForm" class="options-form" method="get" action="">
        <div class="options">
            <span style="margin-right: 16px;">
                <select id="quarter" name="quarter">
                    <option value="">全クール</option>
                    {foreach from=$quarters item=item}
                        <option value="{$item}" {if $quarter|default:'' === $item}selected{/if}>{htmlspecialchars($item)}</option>
                    {/foreach}
                </select>
            </span>
            <span style="margin-right: 16px;">
                <input id="searchFilter" type="text" name="filter" placeholder="タイトルで絞り込めます" value="{$filter}">
                <input type="submit" value="🔍">
                <input type="button" value="❌" onclick="$('#searchFilter').val('');">
            </span>
            <input type="checkbox" id="hiddenWatched" onchange="window.filter();"><label for="hiddenWatched">未視聴のみ表示</label>

        </div>
    </form>
    {include file="./paginator.tpl"}
    <form id="form" method="post" action="{$root_url}/records/update">
        <input type="hidden" id="watchLogId" name="watchLogId" value=""/>
        <input type="hidden" id="newValue" name="newValue" value=""/>
        <input type="hidden" id="filterValue" name="filterValue" value=""/>
        <input type="hidden" id="quarterValue" name="quarterValue" value=""/>
        <input type="hidden" id="pageValue" name="pageValue" value=""/>
        <table class="grid">
            <thead>
            <tr>
                <th>タイトル</th>
                <th>放送日時</th>
                <th>XSPF</th>
                <th>RAW</th>
                <th>視聴記録</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$data item=rec}
                <tr>
                    <td>
                        {if $rec->duplicatedCount > 1 || count($rec->conflicts) > 1}<span class="record-warning" onclick="showWarning()" data-dup="{$rec->duplicatedCount}" data-conflicts="{count($rec->conflicts)}">ℹ️</span>{/if}
                        {htmlspecialchars($rec->title)}
                        {if !empty($rec->subTitle) || !empty($rec->episode)}<br>{/if}
                        {if !empty($rec->episode)}<span class="episode"> #{htmlspecialchars($rec->episode)}</span>{/if}
                        {if !empty($rec->subTitle)}<span class="subtitle"> {htmlspecialchars($rec->subTitle)}</span>{/if}
                    </td>
                    <td>{$rec->date->format('Y/m/d H:i:s')}<br><span class="channel-name">{htmlspecialchars($rec->channelName)}</span></td>
                    <td><a href="{$root_url}/records/stream?type=xspf&container=m2ts&id={urlencode($rec->programId)}">M2TS</a> <a href="{$root_url}/records/stream?type=xspf&container=mp4&id={urlencode($rec->programId)}">MP4</a></td>
                    <td><a href="{$root_url}/records/stream?type=raw&container=m2ts&id={urlencode($rec->programId)}">M2TS</a> <a href="{$root_url}/records/stream?type=raw&container=mp4&id={urlencode($rec->programId)}">MP4</a></td>
                    <td align="center">{if $rec->watched}<a class="btn-watched" href="#" onclick="update('{','|implode:$rec->getWatchLogId()}', '0'); return false;">視聴済</a>{else}<a class="btn-notyet" href="#"  onclick="update('{','|implode:$rec->getWatchLogId()}', '1'); return false;">未視聴</a>{/if}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="5"><span class="msg-info">録画データが見つかりませんでした</span></td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </form>
    {include file="./paginator.tpl"}
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{$root_url}/js/records/index.js" type="text/javascript"></script>
</body>
</html>
