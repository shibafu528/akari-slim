<?php
declare(strict_types=1);

namespace Akari\Utility;

class EnvUtil
{
    public static function isDebug(): bool
    {
        $value = $_ENV['APP_DEBUG'];
        if ($value === 'false') {
            return false;
        }
        return (bool)$_ENV['APP_DEBUG'];
    }
}
