<?php
declare(strict_types=1);

namespace Akari\Contract;

use Psr\Http\Message\ResponseInterface;

interface ViewRenderer
{
    public function render(ResponseInterface $response, string $template, array $data = []): ResponseInterface;
}
