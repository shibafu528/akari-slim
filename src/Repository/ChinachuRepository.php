<?php
declare(strict_types=1);

namespace Akari\Repository;

use Akari\Api\Chinachu;
use Akari\Api\ChinachuException;
use Akari\Entity\ChinachuRecord;
use DateTime;
use DateTimeZone;

class ChinachuRepository
{
    /** @var Chinachu */
    private $chinachu;

    public function __construct(Chinachu $chinachu)
    {
        $this->chinachu = $chinachu;
    }

    /**
     * @param string $programId
     * @return ChinachuRecord
     * @throws \Akari\Api\ChinachuException
     */
    public function findRecord(string $programId): ChinachuRecord
    {
        return self::parseRecordJsonItem($this->chinachu->get("/recorded/{$programId}.json"));
    }

    /**
     * @throws ChinachuException
     */
    public function findByWatchLogId(int $networkId, int $serviceId, int $eventId, int $startedAt): ?ChinachuRecord
    {
        $watchLogId = [$networkId, $serviceId, $eventId, $startedAt];
        $data = $this->chinachu->get('/recorded.json');
        foreach ($data as $item) {
            $record = self::parseRecordJsonItem($item);
            if ($record->getWatchLogId() == $watchLogId) {
                return $record;
            }
        }
        return null;
    }

    /**
     * @return ChinachuRecord[]
     * @throws \Akari\Api\ChinachuException
     */
    public function findRecords(): array
    {
        $records = [];
        $conflicts = [];

        $data = $this->chinachu->get('/recorded.json');
        foreach ($data as $item) {
            $record = self::parseRecordJsonItem($item);

            $lastIndex = count($records) - 1;
            if ($lastIndex >= 0 && $records[$lastIndex]->isSame($record)) {
                $records[$lastIndex]->duplicatedCount++;
                continue;
            }

            if (empty($conflicts[$record->programId])) {
                $conflicts[$record->programId] = [];
            }
            $conflicts[$record->programId][] = $record;
            $record->conflicts =& $conflicts[$record->programId];

            $records[] = $record;
        }

        return $records;
    }

    public function existsFile(string $programId): bool
    {
        try {
            $this->chinachu->get("/recorded/{$programId}/file.json");
            return true;
        } catch (ChinachuException $ex) {
            if ($ex->getCode() == 404 || $ex->getCode() == 410) {
                return false;
            }
            throw $ex;
        }
    }

    private static function parseRecordJsonItem(array $item)
    {
        $datetime = new DateTime('now', new DateTimeZone('Asia/Tokyo'));
        $datetime->setTimestamp($item['start'] / 1000);
        return new ChinachuRecord($item['id'], $item['title'], $item['subTitle'], $item['episode'], $datetime, $item['channel']['name']);
    }
}
