<?php
declare(strict_types=1);

namespace Akari\Repository;

use Akari\Entity\Record;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;

class RecordRepository
{
    /** @var EntityManager */
    private $entityManager;
    /** @var ObjectRepository<Record> */
    private $repository;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Record::class);
    }

    public function find(string $programId, int $userId = 0): ?Record
    {
        return $this->repository->find(compact('programId', 'userId'));
    }

    /**
     * @param int $userId
     * @return Record[]
     */
    public function findAll(int $userId = 0): array
    {
        return $this->repository->findBy(['userId' => $userId]);
    }

    public function update(Record $record)
    {
        $this->entityManager->persist($record);
        $this->entityManager->flush();
    }
}
