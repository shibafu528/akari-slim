<?php
declare(strict_types=1);

namespace Akari\Repository;

use Akari\Entity\WatchLog;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;

class WatchLogRepository
{
    private EntityManager $entityManager;
    private ObjectRepository $repository;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(WatchLog::class);
    }

    public function find(int $networkId, int $serviceId, int $eventId, int $startedAt): ?WatchLog
    {
        return $this->repository->findOneBy(compact('networkId', 'serviceId', 'eventId', 'startedAt'));
    }

    /**
     * @return WatchLog[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function update(WatchLog $log)
    {
        $this->entityManager->persist($log);
        $this->entityManager->flush();
    }
}
