<?php
declare(strict_types=1);

namespace Akari\Repository;

use Akari\Entity\Filter;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;

class FilterRepository
{
    /** @var EntityManager */
    private $entityManager;
    /** @var ObjectRepository<Filter> */
    private $repository;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Filter::class);
    }

    public function find(int $id, int $userId = 0): ?Filter
    {
        return $this->repository->findOneBy(compact('id', 'userId'));
    }

    /**
     * @param int $userId
     * @return Filter[]
     */
    public function findAll(int $userId = 0): array
    {
        return $this->repository->findBy(['userId' => $userId]);
    }

    public function update(Filter $filter)
    {
        $this->entityManager->persist($filter);
        $this->entityManager->flush();
    }

    public function delete(Filter $filter)
    {
        $this->entityManager->remove($filter);
        $this->entityManager->flush();
    }
}
