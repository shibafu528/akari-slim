<?php
declare(strict_types=1);

namespace Akari\Adapter;

use Akari\Contract\ViewRenderer;
use Akari\Utility\EnvUtil;
use Psr\Http\Message\ResponseInterface;

class Smarty implements ViewRenderer
{
    /** @var \Smarty */
    private $smarty;

    public function __construct()
    {
        $this->smarty = new \Smarty();
        $this->smarty->setTemplateDir(__DIR__ . '/../View');
        $this->smarty->setCompileDir(__DIR__ . '/../../tmp/templates_c');
        $this->smarty->setEscapeHtml(true);
    }

    public function render(ResponseInterface $response, string $template, array $data = []): ResponseInterface
    {
        $this->smarty->assign($data);
        if (EnvUtil::isDebug()) {
            $_ENV['SMARTY_VARS'] = print_r($this->smarty->tpl_vars, true);
        }

        if (preg_match('/\.tpl\z/u', $template) === 0) {
            $template .= '.tpl';
        }
        $response->getBody()->write($this->smarty->fetch($template));
        return $response;
    }

    public function assign(string $key, $value)
    {
        $this->smarty->assign($key, $value);
    }
}
