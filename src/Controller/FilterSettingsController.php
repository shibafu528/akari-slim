<?php
declare(strict_types=1);

namespace Akari\Controller;

use Akari\Entity\Filter;
use Akari\Repository\FilterRepository;
use Slim\App;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class FilterSettingsController extends AbstractController
{
    public function get(Request $request, Response $response, FilterRepository $filterRepository)
    {
        $filters = $filterRepository->findAll();

        return $this->render($response, 'settings/filter', [
            'data' => $filters,
            'target_label' => Filter::TARGET_LABEL,
            'matcher_label' => Filter::MATCHER_LABEL,
        ]);
    }

    public function post(Request $request, Response $response, App $app, FilterRepository $filterRepository)
    {
        // TODO: MethodOverrideMiddlewareを使えば _METHOD パラメータでPUTとか使えるので、そうするのもあり
        $input = filter_var_array($request->getParsedBody() ?? [], [
            'filterId' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_NULL_ON_FAILURE],
            'delete' => ['filter' => FILTER_VALIDATE_BOOLEAN, 'flags' => FILTER_NULL_ON_FAILURE],
            'keyword' => FILTER_DEFAULT,
            'target' => [
                'filter' => FILTER_VALIDATE_INT,
                'flags' => FILTER_NULL_ON_FAILURE,
                'options' => [
                    'min_range' => min(array_keys(Filter::TARGET_LABEL)),
                    'max_range' => max(array_keys(Filter::TARGET_LABEL))
                ]
            ],
            'matcher' => [
                'filter' => FILTER_VALIDATE_INT,
                'flags' => FILTER_NULL_ON_FAILURE,
                'options' => [
                    'min_range' => min(array_keys(Filter::MATCHER_LABEL)),
                    'max_range' => max(array_keys(Filter::MATCHER_LABEL))
                ]
            ],
        ], true);

        if ($input['delete'] === true) {
            // 削除
            $filter = $filterRepository->find((int)$input['filterId']);
            if ($filter !== null) {
                $filterRepository->delete($filter);
            }
        } else {
            if (empty($input['keyword'])) {
                throw new HttpBadRequestException($request, "Invalid parameter: keyword can't be empty");
            }
            if ($input['target'] === null) {
                throw new HttpBadRequestException($request, "Invalid parameter: target is invalid");
            }
            if ($input['matcher'] === null) {
                throw new HttpBadRequestException($request, "Invalid parameter: matcher is invalid");
            }

            if ($input['filterId'] === null) {
                $filter = new Filter(0);
            } else {
                $filter = $filterRepository->find((int)$input['filterId']);
                if ($filter === null) {
                    throw new HttpNotFoundException($request, "Filter not found (id={$input['filterId']})");
                }
            }
            $filter->setKeyword((string)$input['keyword']);
            $filter->setTarget((int)$input['target']);
            $filter->setMatcher((int)$input['matcher']);
            $filterRepository->update($filter);
        }

        return $response->withStatus(302)
            ->withHeader('Location', $app->getRouteCollector()->getRouteParser()->urlFor('settings.filter.index'));
    }
}
