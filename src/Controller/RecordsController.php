<?php
declare(strict_types=1);

namespace Akari\Controller;

use Akari\Api\ChinachuException;
use Akari\Entity\ChinachuRecord;
use Akari\Entity\CompositeRecord;
use Akari\Entity\Filter;
use Akari\Entity\Record;
use Akari\Entity\WatchLog;
use Akari\Repository\ChinachuRepository;
use Akari\Repository\FilterRepository;
use Akari\Repository\RecordRepository;
use Akari\Repository\WatchLogRepository;
use DateTime;
use Slim\App;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpException;
use Slim\Exception\HttpNotFoundException;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class RecordsController extends AbstractController
{
    private const ITEMS_PER_PAGE = 50;

    public function index(Request $request, Response $response, ChinachuRepository $chinachuRepository, RecordRepository $recordRepository, WatchLogRepository $watchLogRepository, FilterRepository $filterRepository)
    {
        // クエリを取得
        $params = filter_var_array($request->getQueryParams(), [
            'filter' => FILTER_DEFAULT,
            'quarter' => FILTER_DEFAULT,
            'page' => FILTER_VALIDATE_INT,
        ], true);
        $query = (string)$params['filter'];
        $quarter = (string)$params['quarter'];
        $splitQuarter = explode('Q', $quarter);
        $page = (int)$params['page'];
        if ($page === 0) {
            $page = 1;
        }

        // データを取得
        $dbRecords = [];
        foreach ($recordRepository->findAll() as $record) {
            $dbRecords[$record->getProgramId()] = $record;
        }
        $watchLogs = [];
        foreach ($watchLogRepository->findAll() as $watchLog) {
            $watchLogs[$watchLog->getIdentifier()] = $watchLog;
        }
        $records = array_map(function (ChinachuRecord $record) use ($dbRecords, $watchLogs) {
            $wlIdentifier = implode(',', $record->getWatchLogId());
            return new CompositeRecord($record, $dbRecords[$record->programId] ?? null, $watchLogs[$wlIdentifier] ?? null);
        }, $chinachuRepository->findRecords());
        $filters = $filterRepository->findAll();

        // 四半期リストを生成
        $quarters = $this->getQuarterList($records);

        // フィルタ処理を行う
        $records = array_filter($records, function($v) use ($query, $splitQuarter, $filters) {
            // フィルタ設定を用いた判定
            foreach ($filters as $filter) {
                $target = '';

                // 検査対象を取得
                switch ($filter->getTarget()) {
                    case Filter::TARGET_TITLE:
                        $target = $v->title;
                        break;
                }

                // 判定を実行
                if ($filter->match($target) === false) {
                    return false;
                }
            }

            // 四半期クエリを用いた判定
            if (!empty($splitQuarter) && count($splitQuarter) === 2) {
                if ($v->date->format('Y') !== $splitQuarter[0] || ($this->getQuarter($v->date) + 1) !== (int) $splitQuarter[1]) {
                    return false;
                }
            }

            // 検索クエリを用いた判定
            if (!empty($query)) {
                return mb_strpos($v->title, $query) !== false;
            }

            return true;
        });

        // 録画開始日時逆順ソート
        uasort($records, function ($lhs, $rhs) {
            if ($lhs->date == $rhs->date) {
                return 0;
            }
            return ($lhs->date < $rhs->date) ? 1 : -1;
        });

        // 件数を数えておく
        $recordsCount = count($records);
        $notWatchedCount = count(array_filter($records, function ($x) { return !$x->watched; }));

        // ページネーション
        $records = array_slice($records, ($page - 1) * self::ITEMS_PER_PAGE, self::ITEMS_PER_PAGE);

        $data = [
            'count_all' => $recordsCount,
            'count_notyet' => $notWatchedCount,
            'page' => $page,
            'page_count' => ceil($recordsCount / self::ITEMS_PER_PAGE),
            'data' => $records,
            'filter' => $query,
            'quarter' => $quarter,
            'quarters' => $quarters,
        ];
        return $this->render($response, 'records/index', $data);
    }

    public function conflicts(Request $request, Response $response, ChinachuRepository $chinachuRepository, RecordRepository $recordRepository, WatchLogRepository $watchLogRepository)
    {
        // データを取得
        $dbRecords = [];
        foreach ($recordRepository->findAll() as $record) {
            $dbRecords[$record->getProgramId()] = $record;
        }
        $watchLogs = [];
        foreach ($watchLogRepository->findAll() as $watchLog) {
            $watchLogs[$watchLog->getIdentifier()] = $watchLog;
        }
        $records = array_map(function (ChinachuRecord $record) use ($dbRecords, $watchLogs) {
            $wlIdentifier = implode(',', $record->getWatchLogId());
            return new CompositeRecord($record, $dbRecords[$record->programId] ?? null, $watchLogs[$wlIdentifier] ?? null);
        }, $chinachuRepository->findRecords());

        // 重複チェック
        $conflicts = [];
        foreach ($records as $index => $record) {
            if (empty($conflicts[$record->programId])) {
                $conflicts[$record->programId] = [];
            }

            $size = count($conflicts[$record->programId]);
            if ($size === 0 || !$conflicts[$record->programId][$size - 1]->isSame($record)) {
                $conflicts[$record->programId][] = $record;
            }
        }
        foreach ($conflicts as $id => $recs) {
            if (count($recs) <= 1) {
                unset($conflicts[$id]);
            }
        }

        $data = [
            'data' => $conflicts,
        ];
        return $this->render($response, 'records/conflicts', $data);
    }

    public function stream(Request $request, Response $response, ChinachuRepository $chinachuRepository)
    {
        // クエリを取得
        $params = filter_var_array($request->getQueryParams(), [
            'type' => FILTER_DEFAULT,
            'id' => FILTER_DEFAULT,
            'container' => FILTER_DEFAULT,
        ], true);
        $type = (string)$params['type'];
        $id = (string)$params['id'];
        $container = (string)$params['container'];

        // タイプ指定がない場合はRAW
        if (empty($type)) {
            $type = 'raw';
        }

        // コンテナ指定がない場合はTS
        if (empty($container)) {
            $container = 'm2ts';
        }

        // ID指定がない場合は404返して打ち切り
        if (empty($id)) {
            throw new HttpNotFoundException($request);
        }

        // ファイル存在チェック
        if (!$chinachuRepository->existsFile($id)) {
            throw new HttpException($request, 'Gone', 410);
        }

        // Chinachuサーバのエンドポイントと、パススルー出力時のHTTP Headerを決定する
        $headers = [];
        $endpoint = null;
        if ($type === 'raw') {
            $headers['Transfer-Encoding'] = 'chunked';
            switch ($container) {
                case 'mp4':
                    $headers['Content-Type'] = 'video/mp4';
                    $params = [
                        'ext' => 'mp4',
                        'c:v' => 'libx264',
                        'b:v' => '1M',
                        'c:a' => 'libfdk_aac',
                        'b:a' => '96k',
                        's' => '1280x720',
                        'ss' => '0'
                    ];
                    $endpoint = $_ENV['CHINACHU_API'] . "/recorded/{$id}/watch.mp4?" . http_build_query($params);
                    break;
                case 'm2ts':
                    $headers['Content-Type'] = 'video/MP2T';
                    $endpoint = $_ENV['CHINACHU_API'] . "/recorded/{$id}/watch.m2ts";
                    break;
            }
        } else {
            $headers['Content-Type'] = 'application/octet-stream';
            $headers['Content-Disposition'] = "attachment; filename={$id}.xspf";
            $endpoint = $_ENV['CHINACHU_API'] . "/recorded/{$id}/watch.xspf?";
            switch ($container) {
                case 'mp4':
                    $params = [
                        'ext' => 'mp4',
                        'c:v' => 'libx264',
                        'b:v' => '1M',
                        'c:a' => 'libfdk_aac',
                        'b:a' => '96k',
                        's' => '1280x720',
                        'prefix' => $_ENV['CHINACHU_API'] . "/recorded/{$id}/"
                    ];
                    $endpoint .= http_build_query($params);
                    break;
                case 'm2ts':
                    $params = [
                        'ext' => 'm2ts',
                        'c:v' => 'copy',
                        'c:a' => 'copy',
                        'prefix' => $_ENV['CHINACHU_API'] . "/recorded/{$id}/"
                    ];
                    $endpoint .= http_build_query($params);
                    break;
            }
        }

        if (empty($endpoint)) {
            throw new HttpNotFoundException($request);
        }

        foreach ($headers as $key => $value) {
            $response = $response->withHeader($key, $value);
        }
        $response = $response->withBody((new StreamFactory())->createStreamFromResource(fopen($endpoint, 'rb')));

        return $response;
    }

    public function update(Request $request, Response $response, App $app, ChinachuRepository $chinachuRepository, WatchLogRepository $watchLogRepository)
    {
        // クエリを取得
        $params = filter_var_array($request->getParsedBody() ?? [], [
            'watchLogId' => FILTER_DEFAULT,
            'newValue' => ['filter' => FILTER_VALIDATE_BOOLEAN, 'flags' => FILTER_NULL_ON_FAILURE],
            'filterValue' => FILTER_DEFAULT,
            'quarterValue' => FILTER_DEFAULT,
            'pageValue' => FILTER_DEFAULT,
            'redirectTo' => FILTER_DEFAULT,
        ], true);

        // パラメータが不足している場合は打ち切り
        if ($params['watchLogId'] === '' || $params['newValue'] === null) {
            return $response->withStatus(302)
                ->withHeader('Location', $app->getRouteCollector()->getRouteParser()->urlFor('records.index'));
        }

        $watchLogId = explode(',', $params['watchLogId']);
        if (count($watchLogId) !== 4) {
            throw new HttpBadRequestException($request, 'invalid watchLogId');
        }
        $watchLogId = array_map('intval', $watchLogId);

        $origin = $chinachuRepository->findByWatchLogId(...$watchLogId);
        if ($origin === null) {
            throw new HttpNotFoundException($request);
        }

        $watchLog = $watchLogRepository->find(...$origin->getWatchLogId());
        if ($watchLog === null) {
            $watchLog = new WatchLog(...$origin->getWatchLogId());
            $watchLog->setChinachuId($origin->programId);
        }

        $watchLog->setWatched($params['newValue']);
        $watchLogRepository->update($watchLog);

        // リダイレクトして終了
        if (!empty($params['redirectTo']) && strpos($params['redirectTo'], '/records/conflicts') === 0) {
            $redirectTo = $app->getRouteCollector()->getRouteParser()->urlFor('records.conflicts');
            $fragment = parse_url($params['redirectTo'], PHP_URL_FRAGMENT);
            if (!empty($fragment)) {
                $redirectTo .= '#' . $fragment;
            }
            return $response->withStatus(302)
                ->withHeader('Location', $redirectTo);
        } else {
            $query = [];
            if (!empty($params['filterValue'])) {
                $query['filter'] = $params['filterValue'];
            }
            if (!empty($params['quarterValue'])) {
                $query['quarter'] = $params['quarterValue'];
            }
            if (!empty($params['pageValue'])) {
                $query['page'] = $params['pageValue'];
            }

            return $response->withStatus(302)
                ->withHeader('Location', $app->getRouteCollector()->getRouteParser()->urlFor('records.index', [], $query));
        }
    }

    /**
     * 四半期を表す文字列を取得します。
     * @param DateTime $dateTime 対象のDateTime
     * @return int 四半期 (0～3)
     */
    private function getQuarter(DateTime $dateTime): int
    {
        $month = (int)$dateTime->format('n');
        return (int)(($month - 1) / 3);
    }

    /**
     * 録画データから四半期のリストを生成します。
     * @param ChinachuRecord[] $records 録画データ
     * @return string[] 四半期のリスト
     */
    private function getQuarterList(array $records): array
    {
        $first = reset($records)->date;
        $last = end($records)->date;
        if ($first < $last) {
            $begin = $first;
            $end = $last;
        } else {
            $begin = $last;
            $end = $first;
        }

        $currentYear = (int) $begin->format('Y');
        $currentQuarter = $this->getQuarter($begin);
        $breakYearAndQuarter = $end->format('Y') . $this->getQuarter($end);

        $quarters = [];
        while ($currentYear . $currentQuarter <= $breakYearAndQuarter) {
            $quarters[] = $currentYear . 'Q' . ($currentQuarter + 1);

            $currentQuarter += 1;
            if ($currentQuarter > 3) {
                $currentYear += 1;
                $currentQuarter = 0;
            }
        }

        rsort($quarters);
        return $quarters;
    }
}
