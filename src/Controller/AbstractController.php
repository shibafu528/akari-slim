<?php
declare(strict_types=1);

namespace Akari\Controller;

use Akari\Contract\ViewRenderer;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractController
{
    /** @var ContainerInterface */
    protected $container;

    /** @var ViewRenderer */
    private $viewRenderer;

    public function __construct(ContainerInterface $container, ViewRenderer $viewRenderer)
    {
        $this->container = $container;
        $this->viewRenderer = $viewRenderer;
    }

    public function render(ResponseInterface $response, string $template, array $data = []): ResponseInterface
    {
        return $this->viewRenderer->render($response, $template, $data);
    }
}
