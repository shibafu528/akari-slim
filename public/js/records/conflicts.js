/**
 * POST /records/update
 * @param {string} watchLogId
 * @param {string} newValue
 * @param {string} chinachuId
 */
function update(watchLogId, newValue, chinachuId) {
    $('#watchLogId').val(watchLogId);
    $('#newValue').val(newValue);
    $('#redirectTo').val('/records/conflicts#' + encodeURIComponent(chinachuId))
    $('#form').submit();
}
