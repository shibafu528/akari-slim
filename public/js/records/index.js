/**
 * Created by shibafu on 2015/10/12.
 */

/**
 * POST /records/update
 * @param {string} watchLogId
 * @param {string} newValue
 */
function update(watchLogId, newValue) {
    $('#watchLogId').val(watchLogId);
    $('#newValue').val(newValue);
    $('#filterValue').val($('#searchFilter').val());
    $('#quarterValue').val($('#quarter').val());
    $('#pageValue').val(new URL(location.href).searchParams.get('page'));
    $('#form').submit();
}

function filter() {
    var isHiddenWatched = $('#hiddenWatched').prop('checked');
    $('.grid tr').each(function () {
        if (isHiddenWatched && $(this).find('.btn-watched').length > 0) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });
    sessionStorage.setItem('hiddenWatched', isHiddenWatched);
}

function showWarning() {
    var msg = [];

    var dupCount = event.target.dataset.dup;
    if (dupCount > 1) {
        msg.push('recorded.json内に' + dupCount + '件、同一内容のレコード重複があります。');
    }

    var conflictCount = event.target.dataset.conflicts;
    if (conflictCount > 1) {
        msg.push('recorded.json内に' + conflictCount + '件、同一プログラムIDの重複があります。');
    }

    if (msg.length > 0) {
        alert(msg.join("\n"));
    }
}

$(function () {
    $('#hiddenWatched').prop('checked', sessionStorage.getItem('hiddenWatched') === 'true');
    filter();
});
