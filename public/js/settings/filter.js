/**
 * Created by shibafu on 2016/07/24.
 */

"use strict";

function deleteFilter(id) {
    if (confirm('#' + id + ' を本当に削除しますか？\nこの操作は元に戻せません。')) {
        $('#filterId').val(id);
        $('#delete').val(1);
        $('#form').submit();
    }
}

function editFilter(id, keyword, target, matcher) {
    $('#filterId').val(id);
    $('#keyword').val(keyword);
    $('#target').val(target);
    $('#matcher').val(matcher);

    $('#formTitle').text('#' + id + ' を編集');
    $('#keyword').focus();
    $('#cancelButton').show();
}

function cancelEdit() {
    $('#filterId').val(0);
    $('#keyword').val('');
    $('#target').val(0);
    $('#matcher').val(0);

    $('#formTitle').text('登録');
    $('#cancelButton').hide();
}