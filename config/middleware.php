<?php
declare(strict_types=1);

use Akari\Middleware\SmartyMiddleware;
use Akari\Utility\EnvUtil;
use Slim\App;
use Zeuxisoo\Whoops\Slim\WhoopsMiddleware;

return static function (App $app) {
    // Routing
    $app->addRoutingMiddleware();

    // Error Handler
    if (EnvUtil::isDebug()) {
        $app->add(new WhoopsMiddleware());
        register_shutdown_function(static function () {
            $error = error_get_last();
            if ($error === null) {
                return;
            }
            var_dump($error);
        });
    } else {
        $app->addErrorMiddleware(false, true, true);
        register_shutdown_function(static function () {
            $error = error_get_last();
            if ($error === null) {
                return;
            }
            header('HTTP/1.1 500 Internal Server Error');
            echo '＼（し・ω・だ）／';
        });
    }

    // Template Engine
    $app->add(new SmartyMiddleware($app));
};
