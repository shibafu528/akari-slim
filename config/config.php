<?php
declare(strict_types=1);

use Akari\Adapter\Smarty;
use Akari\Api\Chinachu;
use Akari\Api\ChinachuApi;
use Akari\Api\ChinachuCache;
use Akari\Api\ChinachuRecordedJson;
use Akari\Contract\ViewRenderer;
use Akari\Utility\EnvUtil;
use DI\FactoryInterface;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

return [
    ViewRenderer::class => DI\create(Smarty::class),

    EntityManager::class => function (ContainerInterface $container) {
        $config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
            [__DIR__ . '/../src/Entity'],
            EnvUtil::isDebug(),
            null,
            null,
            false
        );
        return EntityManager::create(['url' => $container->get('database.url')], $config);
    },

    Chinachu::class => function (FactoryInterface $factory) {
        if (!empty($_ENV['RECORDED_JSON'])) {
            return $factory->make(ChinachuRecordedJson::class, ['filePath' => $_ENV['RECORDED_JSON']]);
        }

        if (extension_loaded('apcu')) {
            $useApcu = apcu_enabled();
        } else {
            $useApcu = false;
        }

        if ($useApcu) {
            return $factory->make(ChinachuCache::class);
        } else {
            return $factory->make(ChinachuApi::class);
        }
    },

    'database.url' => $_ENV['DB_URL'],
    'timezone' => 'Asia/Tokyo',
];
