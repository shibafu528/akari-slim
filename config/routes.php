<?php
declaRe(strict_types=1);

use Akari\Controller\FilterSettingsController;
use Akari\Controller\RecordsController;
use Slim\App;

return static function (App $app) {
    $redirector = function ($to) use ($app) {
        return function (\Slim\Psr7\Request $request, \Slim\Psr7\Response $response) use ($app, $to) {
            return $response->withStatus(302)->withHeader('Location', $app->getBasePath() . $to);
        };
    };

    $app->get('/', $redirector('/records'));
    $app->get('/records', [RecordsController::class, 'index'])->setName('records.index');
    $app->get('/records/stream', [RecordsController::class, 'stream']);
    $app->post('/records/update', [RecordsController::class, 'update']);
    $app->get('/records/conflicts', [RecordsController::class, 'conflicts'])->setName('records.conflicts');
    $app->get('/settings', $redirector('/settings/filter'));
    $app->get('/settings/filter', [FilterSettingsController::class, 'get'])->setName('settings.filter.index');
    $app->post('/settings/filter', [FilterSettingsController::class, 'post']);
};
