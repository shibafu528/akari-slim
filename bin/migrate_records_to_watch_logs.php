#!/usr/bin/env php
<?php
declare(strict_types=1);

use Akari\Entity\WatchLog;
use Akari\Repository\ChinachuRepository;
use Akari\Repository\RecordRepository;
use Akari\Repository\WatchLogRepository;
use Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

function bootstrap(): \DI\Container
{
    // Load .env
    $dotenv = Dotenv::createImmutable(__DIR__ . '/../');
    $dotenv->safeLoad();

    // Init container
    $containerBuilder = new \DI\ContainerBuilder();
    $containerBuilder->addDefinitions(__DIR__ . '/../config/config.php');
    $container = $containerBuilder->build();

    // Set app timezone
    date_default_timezone_set($container->get('timezone'));

    return $container;
}

function echo_log(string $message)
{
    echo '[' . date_format(date_create(), 'Y-m-d H:i:s.u') . '] ' . $message . PHP_EOL;
    flush();
    if (ob_get_level() > 0) {
        ob_flush();
    }
}

function run(\DI\Container $container)
{
    try {
        echo_log("\e[42m" . '==== Start ' . basename(__FILE__, '.php') . ' ====' . "\e[0m");

        /** @var ChinachuRepository $chinachuRepository */
        $chinachuRepository = $container->get(ChinachuRepository::class);
        /** @var RecordRepository $recordRepository */
        $recordRepository = $container->get(RecordRepository::class);
        /** @var WatchLogRepository $watchLogRepository */
        $watchLogRepository = $container->get(WatchLogRepository::class);

        $records = $chinachuRepository->findRecords();
        foreach ($records as $record) {
            $rec = $recordRepository->find($record->programId);
            if ($rec === null) {
                echo_log(sprintf('%s: not exists in database, skip.', $record->programId));
                continue;
            }

            $wl = $watchLogRepository->find($record->id->networkId, $record->id->serviceId, $record->id->eventId, $record->date->getTimestamp());
            if ($wl !== null) {
                echo_log(sprintf("\e[33m" . '%s -> {%d,%d,%d,%d}: watch log already exists, skip.' . "\e[0m", $record->programId, $record->id->networkId, $record->id->serviceId, $record->id->eventId, $record->date->getTimestamp()));
                continue;
            }

            $wl = new WatchLog($record->id->networkId, $record->id->serviceId, $record->id->eventId, $record->date->getTimestamp());
            $wl->setChinachuId($record->programId);
            $wl->setWatched($rec->isWatched());
            $watchLogRepository->update($wl);

            echo_log(sprintf("\e[32m" . '%s -> {%d,%d,%d,%d}: migrated.' . "\e[0m", $record->programId, $record->id->networkId, $record->id->serviceId, $record->id->eventId, $record->date->getTimestamp()));
        }
    } finally {
        echo_log("\e[42m" . '==== End ' . basename(__FILE__, '.php') . ' ====' . "\e[0m");
    }
}

run(bootstrap());
