# maintenance commands for docker environment

# Setup environment
.PHONY: init
init:
	$(MAKE) fix
	$(MAKE) migrate
	composer install

# Run database migration
.PHONY: migrate
migrate:
	migrate -path migrate -database "mysql://akari:akari@tcp(db:3306)/akari" up

# Fix filesystem permission
.PHONY: fix
fix:
	chown -R www-data:www-data tmp
