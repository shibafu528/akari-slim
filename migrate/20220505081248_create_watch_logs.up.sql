CREATE TABLE watch_logs (
    network_id int NOT NULL,
    service_id int NOT NULL,
    event_id int NOT NULL,
    start_at bigint NOT NULL COMMENT '番組の開始時刻のUNIXタイムスタンプ',
    chinachu_id varchar(255) COMMENT 'Chinachu上の番組ID',
    watched boolean NOT NULL DEFAULT FALSE COMMENT '視聴済フラグ',
    create_at datetime NOT NULL DEFAULT current_timestamp(),
    update_at datetime NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (network_id, service_id, event_id, start_at)
);

CREATE INDEX index_of_chinachu_id ON watch_logs (chinachu_id) USING BTREE;
