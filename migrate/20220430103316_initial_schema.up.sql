CREATE TABLE `records` (
  `programId` varchar(64) NOT NULL,
  `userId` int(11) NOT NULL,
  `watched` varchar(1) DEFAULT '0',
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `update_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`programId`,`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `keyword` varchar(256) NOT NULL,
  `target` int(1) NOT NULL,
  `matcher` int(1) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `update_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `index_of_id_and_user_id` (`id`,`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
